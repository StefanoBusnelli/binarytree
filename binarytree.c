#include "binarytree.h"

void bintree_create( st_bintree **t ) {
  st_bintree    *lt = NULL;

  if ( t == NULL )
    return;

  lt = ( st_bintree* )calloc( 1, sizeof( st_bintree ) );
  if ( lt != NULL ) {
    lt->data    = NULL;
    lt->weight  = 0;
    lt->left    = NULL;
    lt->right   = NULL;
  }
  *t = lt;
}

void bintree_destroy( st_bintree **t ) {
  st_bintree    *pt = NULL;
  /* Se non è allocato non devo liberare nulla */
  if ( *t == NULL )
    return;
  pt = *t;
  /* Se ci sono dati o nodi collegati vanno prima rimossi per non creare segmentazioni di memoria */
  if ( pt->data != NULL || pt->left != NULL || pt->right != NULL )
    return;

  free( *t );

  *t = NULL;
}

st_bintree *bintree_search( st_bintree *t, void *data, f_bintree_test f_test ) {
  enum en_bt_data test;
  st_bintree        *n = NULL;

  if ( t == NULL || t->data == NULL )
    return NULL;

  /* Controllo con la callback che il dato sia uguale a quello in ingresso */
  test = f_test( t->data, data );
  if ( test == equal )
    return t;
  /* Eseguo bintree_search sul ramo sinistro */
  if ( test == lesser ) {
    n = bintree_search( t->left, data, f_test );
    /* Se ho trovato il dato esco */
    if ( n != NULL )
      return n;
  }
  /* Eseguo bintree_search sul ramo destro */
  if ( test == greater ) {
    n = bintree_search( t->right, data, f_test );
    /* Se ho trovato il dato esco */
    if ( n != NULL )
      return n;
  }

  return NULL;
}

st_bintree *bintree_insert( st_bintree *t, void *data, f_bintree_test f_test ) {
  enum en_bt_data test;
  st_bintree        *n = NULL;

  if ( t == NULL )
    return NULL;

  /* Se non t contiene dati, assegno il dato ed esco */
  if ( t->data == NULL ) {
    t->data = data;
    return t;
  } else {
  /* Se t contiene un dato eseguo il confronto */
    test = f_test( t, data );
    /* Se data è minore di t->data inserisco nel nodo sinistro */
    if ( test == lesser ) {
      /* Se non esiste creo il ramo sinistro */
      if ( t->left == NULL ) 
        bintree_create( &(t->left) );
      /* Eseguo l'inserimento nel ramo destro */
      if ( t->left != NULL )
        n = bintree_insert( t->left, data, f_test );
    }
    if ( test == equal || test == greater ) {
      /* Se non esiste creo il ramo destro */
      if ( t->right == NULL )
        bintree_create( &(t->right) );
      /* Eseguo l'inserimento nel ramo destro */
      if ( t->right != NULL )
        n = bintree_insert( t->right, data, f_test );
    }
    /* Se ho creato un sotto nodo, incremento il peso di questo nodo */
    if ( n != NULL )
      t->weight++;
    /* Ritorno il puntatore al nodo che contiene data oppure NULL se data è già presente nell' albero */
    return n;
  }
}

void bintree_explore( st_bintree *t, void *data, f_bintree_explore f_before, f_bintree_explore f_middle, f_bintree_explore f_after ) {
  if ( f_before != NULL )
    f_before( t, data );
  if ( t != NULL )
    bintree_explore( t->left,  data, f_before, f_middle, f_after );  
  if ( f_middle != NULL )
    f_middle( t, data );
  if ( t != NULL )
    bintree_explore( t->right, data, f_before, f_middle, f_after );  
  if ( f_after != NULL )
    f_after( t, data );
}

void bintree_dump( st_bintree *t, unsigned int level, enum en_bt_branch branch, f_bintree_dump f_dump, void *data ) {
  /* Esecuzione callback */
  f_dump( t, level, branch, data );
  /* Ramo sinistro */
  if ( t != NULL )
    bintree_dump( t->left,  level + 1, left,  f_dump, data );  
  /* Ramo destro */
  if ( t != NULL )
    bintree_dump( t->right, level + 1, right, f_dump, data );  
}
