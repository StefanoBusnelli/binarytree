GCC=gcc
CFLAGS= -ggdb -Wall
CC=$(GCC) $(CFLAGS)

LIBNAME=binarytree
OBJFILE=obj/$(LIBNAME).o
LDNAME=lib$(LIBNAME).so
STNAME=lib$(LIBNAME).a
SONAME=$(LDNAME).1
REALNAME=$(SONAME).2.0

deps=$(REALNAME)

all: libs

.PHONY: libs doc test clean install help

################################################################################
#
#	Shared library
#

libs: $(OBJFILE) $(deps)

$(OBJFILE): binarytree.c binarytree.h
	$(CC) -c -fPIC -o $@ $< -I.
	@ar rcs $(STNAME) $@

$(REALNAME): $(OBJFILE)
	$(CC) -shared -fPIC -Wl,-soname,$(SONAME) -o $@ $^ -lc
	@ln -f -s $(REALNAME) $(SONAME)
	@ln -f -s $(REALNAME) $(LDNAME)

################################################################################
#
#	Test
#

test: $(deps)
	$(MAKE) all --directory=test

################################################################################
#
#	Clean
#

clean:
	@rm -f $(STNAME) $(LDNAME) $(LDNAME).* obj/*.o
	$(MAKE) clean --directory=test

################################################################################
#
#	Install
#

install:
	@if [ ! -d $(HOME)/include ]; then mkdir $(HOME)/include; fi
	@if [ ! -d $(HOME)/lib ];     then mkdir $(HOME)/lib;     fi
	@cp -v   binarytree.h       $(HOME)/include/
	@cp -v   $(STNAME)    $(HOME)/lib/
	@cp -v   $(REALNAME)  $(HOME)/lib/
	@cp -vlf $(LDNAME)    $(HOME)/lib/
	@cp -vlf $(SONAME)    $(HOME)/lib/

################################################################################
#
#	Doxygen
#

doc:
	@if [ -e /usr/bin/doxygen ]; then doxygen Doxyfile; else echo "doxygen not installed"; fi

################################################################################
#
#	Help
#
help:
	@echo "Targets:"
	@echo "make libs"
	@echo "make install"
	@echo "make doc"
	@echo "make test"
	@echo "make clean"

