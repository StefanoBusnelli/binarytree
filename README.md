# binarytree

This is a general purpose implementation of a dynamic binary tree structure.
Contains API's for:
  * Creating a tree
  * Insert data
  * Find data
  * Explore the tree

Insert, Find and Explore functions uses callbacks for managing the data types that the tree contains, that is a pointer to void.

```
                             |       
                        -----------  
                        |    T    |  
                        -----------  
               ..........|       |..........
               |                           |             
          -----------                 -----------
          |    R    |                 |    L    |
          -----------                 -----------
        ...|       |...             ...|       |
        |             |             |          - 
   -----------   -----------   -----------    
   |    RR   |   |    RL   |   |    LR   |   
   -----------   -----------   -----------   
    |       |     |       |     |       |... 
    -       -     -       -     -          |
                                      -----------
                                      |    LRL  |
                                      -----------
                                       |       | 
                                       -       -    
```

## Available make targets ##

```
make help
```

## Compile ##

```
make libs
```

## Install ##

Install dir is the user's home directory, so $LD_LIBRARY_PATH should be set to include $HOME/lib and applications should search headers in $HOME/include

```
make install
```

## Test ##

```
make test
```

## Docs ##

```
make docs
```
