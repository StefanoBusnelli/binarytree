#ifndef _B_TREE

#define _B_TREE

#include <stdlib.h>

typedef struct st_bintree   st_bintree;
typedef enum   en_bt_data   en_bt_data;
typedef enum   en_bt_branch en_bt_branch;

/**
 *  @brief      Prototipo callback per effettuare i confronti
 *  @detail     Questa callback viene eseguita ad ogni chiamata ricorsiva di insert e di search.
 *              Confronta il dato puntato dal nodo attuale con quello in ingresso secondo le modalità definite nella callback.
 *              Restituisce le informazioni sul fatto che il dato passato siamo minore (lesses), uguale (equal) o maggiore (greater) di quello puntato dal nodo.
 *              Permette anche di definire modalità per scartare il dato in ingresso ( junk )
 *  @param      st_bintree, puntatore al nodo corrente della chiamata ricorsiva
 *  @param      dato, dato da confrontare con quello del nodo attuale
 *  @return     enum en_bt_data
 */
typedef enum en_bt_data ( *f_bintree_test )    ( st_bintree*, void* );

/**
 *  @brief      Prototipo callback per l'esplorazione dell' albero
 *  @detail     Questa callback viene eseguita ad ogni chiamata ricorsiva e permette di implementare del codice che utilizza i dati puntati dal nodo attuale ed un dato facoltativo condiviso da ogni chiamata.
 *              E' possibile così stampare a video i dati contenuti nel nodo, calcolare statistiche utilizzando il dato opzionale ecc...
 *              Viene eseguita anche su nodi NULL, durante la chiamata ricorsiva ai nodi dell' albero t->left e/o t->right che sono NULL
 *  @param      st_bintree
 *  @param      dato
 */
typedef void            ( *f_bintree_explore ) ( st_bintree*, void* );

/**
 *  @brief      Prototipo callback per la visualizzazione grafica dell' albero
 *  @detail     Questa callback viene eseguita ad ogni chiamata ricorsiva e permette di implementare del codice che utilizza i dati puntati dal nodo attuale, il livello ricorsivo raggiunto, il lato del ramo ed un dato facoltativo condiviso da ogni chiamata.
 *              E' possibile così stampare a video la struttura dell' albero
 *              Viene eseguita anche su nodi NULL, durante la chiamata ricorsiva ai nodi dell' albero t->left e/o t->right che sono NULL
 *  @param      st_bintree
 *  @param      livello ricorsivo raggiunto
 *  @param      lato dell' albero
 *  @param      dato
 */
typedef void            ( *f_bintree_dump ) ( st_bintree*, unsigned int, enum en_bt_branch, void* );

/**
 *  @brief  Struct nodo albero binario
 */
typedef struct st_bintree {
  void          *data;      /**<    Puntatore al dato memorizzato */
  unsigned int  weight;     /**<    Numero di nodi collegati a questo nodo, viene aggiornato ricorsivamente ad ogni modifica della struttura. */
  st_bintree    *left;      /**<    Puntatore al ramo sinistro    */
  st_bintree    *right;     /**<    Puntatore al ramo destro      */
} t_st_bintree;

/**
 *  @brief  Enum con i valori di esito dei test di confronto tra i dati
 */
enum en_bt_data {
  junk,                     /**<    Dato da scartare                 */
  lesser,                   /**<    Dato minore di quello presente   */
  equal,                    /**<    Dato uguale a  quello presente   */
  greater                   /**<    Dato maggiore di quello presente */
};

/**
 *  @brief  Enum con i valori descrittivi del lato del ramo
 */
enum en_bt_branch {
  root,                     /**<    Nodo radice                      */
  left,                     /**<    Dato sinistro                    */
  right                     /**<    Dato destro                      */
};

/**
 *  @brief      Create
 *  @detail     Crea un nodo vuoto.
 *  @param      **t         Indirizzo del puntatore all' albero.
 *  @returns    *t          Puntatore al nodo.
 */
void bintree_create( st_bintree **t );

/**
 *  @brief      Destroy
 *  @detail     Se non ci sono dati o nodi collegati, ibera la memoria occupata da *t e lo assegna a NULL
 *  @param      **t         Indirizzo del puntatore all' albero.
 *  @returns    *t          Puntatore al nodo. Se è stato eliminato vale NULL
 */
void bintree_destroy( st_bintree **t );

/**
 *  @brief      Search
 *  @detail     Cerca un dato nell' albero e restituisce il puntatore al nodo che lo contiene.
 *  @param      *t          Puntatore all' albero binario.
 *  @param      *data       Dato da cercare nell' albero
 *  @param      f_test      Callback con cui effettuare i confronti
 *  @return     st_bintree* Puntatore al nodo contenente il dato oppure NULL
 */
st_bintree *bintree_search( st_bintree *t, void *data, f_bintree_test f_test );

/**
 *  @brief      Insert
 *  @detail     Inserisce un dato nell' albero
 *  @param      *t          Puntatore all' albero binario.
 *  @param      *data       Dato da inserire nell' albero
 *  @param      f_test      Callback con cui effettuare i confronti
 *  @return     st_bintree* Puntatore al nodo inserito oppure NULL
 */
st_bintree *bintree_insert( st_bintree *t, void *data, f_bintree_test f_test );

/**
 *  @brief      Explore
 *  @detail     Esplora l'albero in maniera ordinata eseguendo delle callback opzionali prima delle chiamate ricorsive, tra una e l'altra e dopo.
 *  @param      *t          Puntatore all' albero binario.
 *  @param      *data       Puntatore ad un dato opzionale da usare nella callback
 *  @param      f_before    Callback con cui trattare i dati, eseguita prima delle chiamate ricorsive ( anche NULL )
 *  @param      f_middle    Callback con cui trattare i dati, eseguita tra le chiamate ricorsive a ->left e ->right ( anche NULL )
 *  @param      f_after     Callback con cui trattare i dati, eseguita dopo le chiamate ricorsive ( anche NULL )
 */
void bintree_explore( st_bintree *t, void *data, f_bintree_explore f_before, f_bintree_explore f_middle, f_bintree_explore f_after );

/**
 *  @brief      Dump
 *  @detail     Esegue la callback con il nodo corrente passando informazioni sul livello ricorsivo raggiunto e sul lato del nodo ( (R)oot, (L)eft, (R)ight )
 *  @param      *t          Puntatore all' albero binario.
 *  @param      level       Livello ricorsivo raggiunto
 *  @param      branch      Lato del ramo
 *  @param      f_explore   Callback da eseguire
 *  @param      *data       Puntatore ad un dato opzionale da usare nella callback
 */
void bintree_dump( st_bintree *t, unsigned int level, enum en_bt_branch branch, f_bintree_dump f_dump, void *data );

#endif
