#include <stdio.h>
#include "binarytree.h"

/* Callbacks */

enum en_bt_data bt_ins_float( st_bintree *t, void *data ) {
  float *pt_data    = NULL;
  float *p_data     = NULL;

  pt_data = ( float* ) t->data;
  p_data  = ( float* ) data;
  if ( *p_data == *pt_data )
    return  junk; /* Scarto il dato se è già presente nell' albero */
  if ( *p_data  < *pt_data )
    return lesser;
  if ( *p_data  > *pt_data )
    return  greater;

  return junk;
}

void dump_tree( st_bintree *t, unsigned int level, enum en_bt_branch branch, void * data ) {
  unsigned int   i = 0;

  for ( i = 0; i < level && branch == right; i++ )
    printf( "%s%19s", ( i == 0 ? " " : "|"), "" );

  if ( t != NULL ) {    
    printf( "%s-(%10p %3d %s)", ( branch == left ? "-" : "+" ), t, level, ( branch == root ? "T" : ( branch == left ? "L" : "R" ) ) );
  } else {
    printf( "%s-(NULL)", ( branch == left ? "-" : "+" ) );
  }

  if ( t == NULL ) 
    printf( "\n" );

  if ( branch == right && t == NULL ) {
    for ( i = 0; i < level; i++ ) 
      printf( "%s%19s", ( i == 0 ? " " : "|"), "" );
    printf( "\n" );
  }
}

/* --- */

int main( int argc, char** argv ) {
  st_bintree    *t = NULL;
  st_bintree    *n = NULL;
  float         d0 = 1.0;
  float         d1 = -0.5;
  float         d2 = 1.5;
  float         d3 = -1.2;
  float         d4 =  2.1;
  float         d5 =  0.4;
  float         d6 = -1.7;
  float         d7 = -0.2;
  float         d8 =  1.9;
  unsigned int   l = 0;

  bintree_create( &t );

  n = bintree_insert( t, &d0, bt_ins_float );
  n = bintree_insert( t, &d1, bt_ins_float );
  n = bintree_insert( t, &d2, bt_ins_float );
  n = bintree_insert( t, &d3, bt_ins_float );
  n = bintree_insert( t, &d4, bt_ins_float );
  n = bintree_insert( t, &d5, bt_ins_float );
  n = bintree_insert( t, &d6, bt_ins_float );
  n = bintree_insert( t, &d7, bt_ins_float );
  n = bintree_insert( t, &d8, bt_ins_float );
  
  printf( "Tree:\n" );
  bintree_dump( t, 0, root, dump_tree, &l );

  return 0;
}

