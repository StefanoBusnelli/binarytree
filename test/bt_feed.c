#include <stdio.h>
#include "binarytree.h"

/* Callbacks */

enum en_bt_data bt_ins_float( st_bintree *t, void *data ) {
  float *pt_data    = NULL;
  float *p_data     = NULL;

  pt_data = ( float* ) t->data;
  p_data  = ( float* ) data;
  if ( *p_data == *pt_data )
    return  junk; /* Scarto il dato se è già presente nell' albero */
  if ( *p_data  < *pt_data )
    return lesser;
  if ( *p_data  > *pt_data )
    return  greater;

  return junk;
}

void print_float( st_bintree *t, void *data ) {
  if ( t == NULL )
    return;

  printf( "Weight: %3d", t->weight );
  if ( t->data != NULL ) {
    printf( "\tData: %8.2f", *( float* )t->data );
  } else {
    printf( "\tData: NULL" );
  }
  printf( "\n" );
}

/* --- */

void print_node( st_bintree *t ) {
  printf( "\t: %16p", t );
  if ( t != NULL && t->data != NULL ) {
    printf( "\t: %8.2f", *( float* )t->data );
  }
  printf( "\n" );
}

int main( int argc, char** argv ) {
  st_bintree    *t = NULL;
  st_bintree    *n = NULL;
  float         d0 = 1.0;
  float         d1 = 0.5;
  float         d2 = 1.5;
  float         d3 = 1.2;
  float         d4 = 1.5;

  printf( "Creo albero\n" );
  bintree_create( &t );
  print_node( t );

  printf( "Inserimento dati\n" );
  n = bintree_insert( t, &d0, bt_ins_float );
  if ( n == NULL ) {
    printf( "Non ho potuto inserire %8.2f\n", d0 );
  } else {
    print_node( n );
  }
  n = bintree_insert( t, &d1, bt_ins_float );
  if ( n == NULL ) {
    printf( "Non ho potuto inserire %8.2f\n", d1);
  } else {
    print_node( n );
  }
  n = bintree_insert( t, &d2, bt_ins_float );
  if ( n == NULL ) {
    printf( "Non ho potuto inserire %8.2f\n", d2 );
  } else {
    print_node( n );
  }
  n = bintree_insert( t, &d3, bt_ins_float );
  if ( n == NULL ) {
    printf( "Non ho potuto inserire %8.2f\n", d3 );
  } else {
    print_node( n );
  }
  n = bintree_insert( t, &d4, bt_ins_float );
  if ( n == NULL ) {
    printf( "Non ho potuto inserire %8.2f\n", d4 );
  } else {
    print_node( n );
  }

  printf( "Stampo albero\n" );
  bintree_explore( t, NULL, NULL, print_float, NULL );

  return 0;
}

