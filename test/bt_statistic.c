#include <stdio.h>
#include "binarytree.h"

/* Callbacks */

enum en_bt_data bt_ins_float( st_bintree *t, void *data ) {
  float *pt_data    = NULL;
  float *p_data     = NULL;

  pt_data = ( float* ) t->data;
  p_data  = ( float* ) data;
  if ( *p_data == *pt_data )
    return  junk; /* Scarto il dato se è già presente nell' albero */
  if ( *p_data  < *pt_data )
    return lesser;
  if ( *p_data  > *pt_data )
    return  greater;

  return junk;
}

void print_float( st_bintree *t, void *data ) {
  if ( t == NULL )
    return;

  printf( "Weight: %3d", t->weight );
  if ( t->data != NULL ) {
    printf( "\tData: %8.2f", *( float* )t->data );
  } else {
    printf( "\tData: NULL" );
  }
  printf( "\n" );
}

typedef struct st_stats {
  unsigned int  num;
  float         sum;
  float         avg;
  float         max;
  float         min;
} t_stats;

void calc_stats( st_bintree *t, void * data ) {
  t_stats   *st = NULL;

  if ( t == NULL )
    return;

  st = ( t_stats* )data;
  st->num++;
  if ( t != NULL && t->data != NULL ) {
    st->sum += *( float* )t->data;
    if ( st->max < *( float* )t->data || st->num == 1 )
      st->max = *( float* )t->data;
    if ( st->min > *( float* )t->data || st->num == 1 )
      st->min = *( float* )t->data;
  }
  st->avg = st->sum / st->num;
}

/* --- */

int main( int argc, char** argv ) {
  st_bintree    *t = NULL;
  st_bintree    *n = NULL;
  float         d0 = 1.0;
  float         d1 = -0.5;
  float         d2 = 1.5;
  float         d3 = -1.2;
  t_stats       st = {0, 0.0, 0.0, 0.0, 0.0};

  bintree_create( &t );

  n = bintree_insert( t, &d0, bt_ins_float );
  n = bintree_insert( t, &d1, bt_ins_float );
  n = bintree_insert( t, &d2, bt_ins_float );
  n = bintree_insert( t, &d3, bt_ins_float );
  
  printf( "Dati inseriti:\n" );
  bintree_explore( t, NULL, NULL, print_float, NULL );

  printf( "Statistiche:\n" );
  bintree_explore( t, &st, NULL, calc_stats, NULL );
  printf( "Num: %8d\nSum: %8.2f\nMin: %8.2f\nMax: %8.2f\nAvg: %8.2f\n", st.num, st.sum, st.min, st.max, st.avg );

  return 0;
}

